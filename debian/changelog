libterm-twiddle-perl (2.73-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libterm-twiddle-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 01:24:32 +0000

libterm-twiddle-perl (2.73-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Carlo Segre from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:49:13 +0100

libterm-twiddle-perl (2.73-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 14:36:29 +0100

libterm-twiddle-perl (2.73-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Add libterm-size-perl to Build-Depends-Indep and Recommends.
    This avoids shelling out to tput which writes warnings to stderr if TERM
    is unset or unknown. (Closes: #784175)
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Mon, 04 May 2015 17:56:54 +0200

libterm-twiddle-perl (2.73-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 2.72
  * Remove debian/libterm-twiddle-perl.docs. Do not install README file.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Florian Schlichting ]
  * Import Upstream version 2.73
  * Update years of upstream copyright
  * Bump dh compatibility to level 8 (no changes necessary)
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0)
  * Rewrite short and long description
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Thu, 19 Sep 2013 23:15:23 +0200

libterm-twiddle-perl (2.71-1) unstable; urgency=low

  * Initial Release.  (Closes: #592197)

 -- Carlo Segre <segre@debian.org>  Sat, 7 Aug 2010 20:51:41 -0500
